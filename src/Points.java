/**
 * Created by Dmitry Vereykin on 7/26/2015.
 */
import javax.swing.*;
import java.awt.*;

public class Points extends JPanel {

    public JLabel dealerLabelPoints;
    public JLabel playerLabelPoints;
    public JLabel dealerPoints;
    public JLabel playerPoints;

    public Points() {
        setLayout(new GridLayout(2, 2));

        dealerLabelPoints = new JLabel(" Dealer:  ");
        dealerPoints = new JLabel("0");
        playerLabelPoints = new JLabel(" Player:   ");
        playerPoints = new JLabel("0");

        add(dealerLabelPoints);
        add(dealerPoints);
        add(playerLabelPoints);
        add(playerPoints);

        setBorder(BorderFactory.createTitledBorder("Points"));

    }

}
