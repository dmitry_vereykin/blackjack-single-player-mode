/**
 * Created by Dmitry Vereykin on 7/26/2015.
 */
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;

public class HandsPanel extends JPanel {
    public JLabel[] dealersLabels;
    public JLabel[] playersLabels;
    public JLabel[] emptyLabels;
    public JButton[] buttonsForAces;
    public ImageIcon blueBackface = (new ImageIcon(getClass().getResource("Cards/Backface_Blue.jpg")));


    public HandsPanel() {
        setLayout(new GridLayout(4, 6));
        File f = new File("Backface_Blue.bmp");

        dealersLabels = new JLabel[6];
        playersLabels = new JLabel[6];
        buttonsForAces = new JButton[6];
        emptyLabels = new JLabel[6];

        for (int index = 0; index < 6; index++) {
            dealersLabels[index] = new JLabel();
            dealersLabels[index].setIcon(blueBackface);
            this.add(dealersLabels[index]);
            dealersLabels[index].setVisible(false);
        }

        for (int index = 0; index < 6; index++) {
            emptyLabels[index] = new JLabel();
            emptyLabels[index].setIcon(blueBackface);
            this.add(emptyLabels[index]);
            emptyLabels[index].setVisible(false);
        }

        for (int index = 0; index < 6; index++) {
            playersLabels[index] = new JLabel();
            playersLabels[index].setIcon(blueBackface);
            this.add(playersLabels[index]);
            playersLabels[index].setVisible(false);
        }

        for (int index = 0; index < 6; index++) {
            buttonsForAces[index] = new JButton("11");
            this.add(buttonsForAces[index]);
            buttonsForAces[index].setVisible(false);
        }

        setBorder(BorderFactory.createTitledBorder("Hands"));
    }

}