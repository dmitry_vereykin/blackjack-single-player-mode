/**
 * Created by Dmitry Vereykin on 7/26/2015.
 */
import javax.swing.*;

public class TopBanner extends JPanel {
    private JLabel topBanner;

    public TopBanner() {
        topBanner = new JLabel("Simplest Blackjack game (pre-alpha v. 0.52) created by Dmitry Vereykin, enjoy!");
        add(topBanner);
    }
}